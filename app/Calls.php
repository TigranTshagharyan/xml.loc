<?php
/**
 * Created by PhpStorm.
 * User: Tigran
 * Date: 7/12/2018
 * Time: 11:35 PM
 */

namespace App;
use Illuminate\Database\Eloquent\Model;

class Calls extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'calls';

    protected $fillable = [
        'name', 'calls',
    ];


}