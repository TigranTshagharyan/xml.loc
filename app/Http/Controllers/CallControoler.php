<?php
/**
 * Created by PhpStorm.
 * User: Tigran
 * Date: 8/12/2018
 * Time: 3:29 PM
 */


namespace App\Http\Controllers;
use App\Calls;

class CallControoler  extends Controller
{
    public function search()
    {
        // find id in xml request
        $xml_call = simplexml_load_file('calls.xml');
        $xml_array  = ($xml_call->xpath("/root"));
        $xml_id = (array)$xml_array[0];
        $id = $xml_id["CallerID"];

        // after check call id write response
        $xml_response = simplexml_load_file('response.xml');
        if(Calls::find($id)){
            $status = $xml_response->addChild('status');
            $status -> addChild('StatusCode', 'ok');
            $status -> addChild('StatusDescription', 'CallerID was found');

            $result = $xml_response->addChild('result');
            $result -> addChild('RoutingID', 3);

            $xml_response->addChild('request', 'HTTP Code: 200');

            $xml_response->asXML('response.xml');

        }else{
            $status = $xml_response->addChild('status');
            $status -> addChild('StatusCode', 'eror');
            $status -> addChild('StatusDescription', 'CallerID was not found');

            $result = $xml_response->addChild('result');
            $result -> addChild('RoutingID', 202);

            $xml_response->addChild('request', 'HTTP Code: 404');

            $xml_response->asXML('response.xml');
        }


    }

}
